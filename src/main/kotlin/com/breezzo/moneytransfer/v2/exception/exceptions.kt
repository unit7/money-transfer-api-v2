package com.breezzo.moneytransfer.v2.exception

import java.lang.RuntimeException

class AccountNotFoundException(accountNumber: String): RuntimeException("Account was not found $accountNumber")

class InvalidTransferDataException(message: String) : RuntimeException(message)