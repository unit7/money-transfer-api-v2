package com.breezzo.moneytransfer.v2.dto

import java.math.BigDecimal

/**
 * @param from account number from which money should be transferred
 * @param to account number to which money should be transferred
 * @param amount amount which should be transferred
 */
data class TransferDetails(val from: String, val to: String, val amount: BigDecimal)