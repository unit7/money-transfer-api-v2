package com.breezzo.moneytransfer.v2.repository

import com.breezzo.moneytransfer.v2.exception.AccountNotFoundException
import com.breezzo.moneytransfer.v2.model.Account
import java.math.BigDecimal

interface AccountRepository {
    fun getAccount(number: String): Account

    fun findAll(): Set<Account>

    /**
     * @return returns special object that can be used to execute atomic update operation on two accounts.
     */
    fun getUpdateExecutor(firstAccountNumber: String, secondAccountNumber: String): UpdateExecutor
}

/**
 * Encapsulates update logic for specific repository implementation
 * (e.g. locking with right order for in memory repository implementation)
 */
interface UpdateExecutor {
    fun executeUpdate(operation: (Account, Account) -> Unit)
}

class InMemoryAccountRepository(
    var accounts: Map<String, Account> = mapOf(
        "123456" to Account("123456", BigDecimal.ZERO),
        "123457" to Account("123457", BigDecimal.ZERO),
        "123458" to Account("123458", BigDecimal.TEN),
        "123459" to Account("123459", BigDecimal.valueOf(100000)),
        "123460" to Account("123460", BigDecimal.valueOf(150)),
        "123461" to Account("123461", BigDecimal.valueOf(500))
    )
) : AccountRepository {

    override fun getAccount(number: String): Account = accounts[number] ?: throw AccountNotFoundException(number)

    override fun findAll(): Set<Account> = accounts.values.toSet()

    override fun getUpdateExecutor(firstAccountNumber: String, secondAccountNumber: String): UpdateExecutor {
        val firstAccount = getAccount(firstAccountNumber)
        val secondAccount = getAccount(secondAccountNumber)

        return object : UpdateExecutor {
            override fun executeUpdate(operation: (Account, Account) -> Unit) {
                val (firstLock, secondLock) = getLocks()
                synchronized(firstLock) {
                    synchronized(secondLock) {
                        operation(firstAccount, secondAccount)
                    }
                }
            }

            private fun getLocks(): Pair<Account, Account> {
                return if (firstAccountNumber < secondAccountNumber) {
                    Pair(firstAccount, secondAccount)
                } else {
                    Pair(secondAccount, firstAccount)
                }
            }
        }
    }
}