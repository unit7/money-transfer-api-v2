package com.breezzo.moneytransfer.v2

import com.breezzo.moneytransfer.v2.controller.AccountController
import com.breezzo.moneytransfer.v2.exception.AccountNotFoundException
import com.breezzo.moneytransfer.v2.exception.InvalidTransferDataException
import com.breezzo.moneytransfer.v2.repository.AccountRepository
import com.breezzo.moneytransfer.v2.repository.InMemoryAccountRepository
import com.breezzo.moneytransfer.v2.service.AccountService
import com.breezzo.moneytransfer.v2.service.MoneyTransferService
import io.javalin.Javalin
import org.slf4j.LoggerFactory

fun main() {
    Application(InMemoryAccountRepository()).init().start(7000)
}

class Application(private val accountRepository: AccountRepository) {

    private val logger = LoggerFactory.getLogger(Application::class.java)

    fun init(): Javalin {
        val transferService = MoneyTransferService(accountRepository)
        val accountService = AccountService(accountRepository)
        val accountController = AccountController(transferService, accountService)
        val app = Javalin.create()

        app.get("/api/account/all", accountController::findAll)
        app.post("/api/account/transfer", accountController::transfer)

        app.exception(AccountNotFoundException::class.java) { e, ctx ->
            logger.trace("Account not found exception occurred for request: {}", ctx.body(), e)
            ctx.status(404).result(e.message!!)
        }

        app.exception(InvalidTransferDataException::class.java) { e, ctx ->
            logger.trace("Invalid amount was specified in request: {}", ctx.body(), e)
            ctx.status(400).result(e.message!!)
        }

        return app
    }
}