package com.breezzo.moneytransfer.v2.model

import java.math.BigDecimal

data class Account(val number: String, var balance: BigDecimal)