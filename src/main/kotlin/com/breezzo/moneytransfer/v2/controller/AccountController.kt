package com.breezzo.moneytransfer.v2.controller

import com.breezzo.moneytransfer.v2.dto.TransferDetails
import com.breezzo.moneytransfer.v2.service.AccountService
import com.breezzo.moneytransfer.v2.service.MoneyTransferService
import io.javalin.http.Context

class AccountController(private val transferService: MoneyTransferService,
                        private val accountService: AccountService) {

    fun findAll(ctx: Context) {
        ctx.json(accountService.findAll())
    }

    fun transfer(ctx: Context) {
        val transferDetails = ctx.bodyValidator<TransferDetails>()
            .check({ it.amount.signum() > 0 }, "Only positive amount is allowed")
            .get()
        transferService.transfer(transferDetails)
    }
}