package com.breezzo.moneytransfer.v2.service

import com.breezzo.moneytransfer.v2.dto.TransferDetails
import com.breezzo.moneytransfer.v2.exception.InvalidTransferDataException
import com.breezzo.moneytransfer.v2.model.Account
import com.breezzo.moneytransfer.v2.repository.AccountRepository
import java.math.BigDecimal

/**
 * Implements money transferring between accounts.
 */
class MoneyTransferService(private val accountRepository: AccountRepository) {
    private val minTransferAmount = BigDecimal("0.01")

    fun transfer(transferDetails: TransferDetails) {
        accountRepository.getUpdateExecutor(transferDetails.from, transferDetails.to).executeUpdate { from, to ->
            transfer(from, to, transferDetails.amount)
        }
    }

    private fun transfer(fromAccount: Account, toAccount: Account, amount: BigDecimal) {
        checkTransferPreconditions(fromAccount, toAccount, amount)
        fromAccount.balance -= amount
        toAccount.balance += amount
    }

    private fun checkTransferPreconditions(from: Account, to: Account, amount: BigDecimal) {
        if (from.number == to.number) {
            throw InvalidTransferDataException("Different accounts must be specified to transfer funds")
        }
        if (amount.signum() <= 0) {
            throw InvalidTransferDataException("Only positive amount allowed")
        }
        if (amount < minTransferAmount) {
            throw InvalidTransferDataException("Minimum allowed amount to transfer is $minTransferAmount")
        }
        if (from.balance < amount) {
            throw InvalidTransferDataException("Insufficient amount on account ${from.number}")
        }
    }
}

class AccountService(private val accountRepository: AccountRepository) {
    fun findAll(): Set<Account> = accountRepository.findAll()
}