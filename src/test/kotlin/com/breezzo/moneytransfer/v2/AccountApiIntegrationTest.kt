package com.breezzo.moneytransfer.v2

import com.breezzo.moneytransfer.v2.dto.TransferDetails
import com.breezzo.moneytransfer.v2.model.Account
import com.breezzo.moneytransfer.v2.repository.InMemoryAccountRepository
import io.javalin.Javalin
import io.restassured.RestAssured
import io.restassured.RestAssured.get
import io.restassured.RestAssured.given
import io.restassured.config.JsonConfig
import io.restassured.config.RestAssuredConfig.newConfig
import io.restassured.http.ContentType
import io.restassured.module.kotlin.extensions.When
import io.restassured.path.json.config.JsonPathConfig
import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.*
import java.math.BigDecimal

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("Account API")
class AccountApiIntegrationTest  {
    private lateinit var app: Javalin

    private val allAccountsUri = "/api/account/all"
    private val transferUri = "/api/account/transfer"

    private val fromAccountBalance = BigDecimal("10.00")
    private val toAccountBalance = BigDecimal("1.00")

    private val fromAccountNumber = "123456"
    private val toAccountNumber = "654321"

    private val accountRepository = InMemoryAccountRepository();

    @BeforeAll
    fun init() {
        app = Application(accountRepository).init().start(7001)
        RestAssured.baseURI = "http://localhost:7001"
        RestAssured.config = newConfig()
            .jsonConfig(JsonConfig.jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL))
    }

    @BeforeEach
    fun setUp() {
        val fromAccount = Account(fromAccountNumber, fromAccountBalance)
        val toAccount = Account(toAccountNumber, toAccountBalance)
        accountRepository.accounts = mapOf(
            fromAccount.number to fromAccount,
            toAccount.number to toAccount
        )
    }

    @AfterAll
    fun tearDown() {
        app.stop()
    }

    @Test
    fun `should transfer amount when balance enough`() {
        val amount = BigDecimal.valueOf(5)
        val transferDetails = TransferDetails(fromAccountNumber, toAccountNumber, amount)

        given()
            .body(transferDetails)
            .contentType(ContentType.JSON)
        .When {
            post(transferUri)
        }
        .then()
            .statusCode(200)

        MatcherAssert.assertThat(getAccountBalance(fromAccountNumber), comparesEqualTo(fromAccountBalance.subtract(amount)))
        MatcherAssert.assertThat(getAccountBalance(toAccountNumber), comparesEqualTo(toAccountBalance.add(amount)))
    }

    @Test
    fun `should transfer amount regardless of arguments order`() {
        val firstAccountNumber = toAccountNumber
        val secondAccountNumber = fromAccountNumber
        val amount = toAccountBalance
        val transferDetails = TransferDetails(firstAccountNumber, secondAccountNumber, amount)

        given()
            .body(transferDetails)
            .contentType(ContentType.JSON)
        .When {
            post(transferUri)
        }
        .then()
            .statusCode(200)

        MatcherAssert.assertThat(getAccountBalance(firstAccountNumber), comparesEqualTo(toAccountBalance.subtract(amount)))
        MatcherAssert.assertThat(getAccountBalance(secondAccountNumber), comparesEqualTo(fromAccountBalance.add(amount)))
    }

    @Test
    fun `should fail when passed account numbers is the same`() {
        val amount = BigDecimal.valueOf(5)
        val transferDetails = TransferDetails(fromAccountNumber, fromAccountNumber, amount)

        given()
            .body(transferDetails)
            .contentType(ContentType.JSON)
        .When {
            post(transferUri)
        }
        .then()
            .statusCode(400)
            .body(`is`("Different accounts must be specified to transfer funds"))

        MatcherAssert.assertThat(getAccountBalance(fromAccountNumber), comparesEqualTo(fromAccountBalance))
    }

    @Test
    fun `should fail when funds is not enough`() {
        val transferDetails = TransferDetails(fromAccountNumber, toAccountNumber, BigDecimal.valueOf(500))

        given()
            .body(transferDetails)
            .contentType(ContentType.JSON)
        .When {
            post(transferUri)
        }
        .then()
            .statusCode(400)
            .body(`is`("Insufficient amount on account $fromAccountNumber"))

        MatcherAssert.assertThat(getAccountBalance(fromAccountNumber), comparesEqualTo(fromAccountBalance))
    }

    @Test
    fun `should fail when fromAccount is not found`() {
        val nonExistentAccountNumber = "111111"
        val transferDetails = TransferDetails(nonExistentAccountNumber, toAccountNumber, BigDecimal.valueOf(5))

        given()
            .body(transferDetails)
            .contentType(ContentType.JSON)
        .When {
            post(transferUri)
        }
        .then()
            .statusCode(404)
            .body(`is`("Account was not found $nonExistentAccountNumber"))
    }

    @Test
    fun `should fail when toAccount is not found`() {
        val nonExistentAccountNumber = "111111"
        val transferDetails = TransferDetails(fromAccountNumber, nonExistentAccountNumber, BigDecimal.valueOf(5))

        given()
            .body(transferDetails)
            .contentType(ContentType.JSON)
        .When {
            post(transferUri)
        }
        .then()
            .statusCode(404)
            .body(`is`("Account was not found $nonExistentAccountNumber"))
    }

    @Test
    fun `should fail when amount is negative`() {
        val transferDetails = TransferDetails(fromAccountNumber, toAccountNumber, BigDecimal.valueOf(-5))

        given()
            .body(transferDetails)
            .contentType(ContentType.JSON)
        .When {
            post(transferUri)
        }
        .then()
            .statusCode(400)
            .body(containsString("Only positive amount is allowed"))
    }

    @Test
    fun `should fail when amount is zero`() {
        val transferDetails = TransferDetails(fromAccountNumber, toAccountNumber, BigDecimal.ZERO)

        given()
            .body(transferDetails)
            .contentType(ContentType.JSON)
        .When {
            post(transferUri)
        }
        .then()
            .statusCode(400)
            .body(containsString("Only positive amount is allowed"))
    }

    @Test
    fun `should fail when amount is less than minimum allowed`() {
        val transferDetails = TransferDetails(fromAccountNumber, toAccountNumber, BigDecimal("0.009"))

        given()
            .body(transferDetails)
            .contentType(ContentType.JSON)
        .When {
            post(transferUri)
        }
        .then()
            .statusCode(400)
            .body(containsString("Minimum allowed amount to transfer is 0.01"))
    }

    @Test
    fun `findAll should return all accounts`() {
        get(allAccountsUri)
        .then()
            .statusCode(200)
            .contentType(ContentType.JSON)
            .body("$", hasSize<Any>(2))
            .body("find { it.number == '$fromAccountNumber' }.balance", comparesEqualTo(fromAccountBalance))
            .body("find { it.number == '$toAccountNumber' }.balance", comparesEqualTo(toAccountBalance))
    }

    @Test
    fun `findAll should return empty collection when no accounts exists`() {
        accountRepository.accounts = emptyMap()
        get(allAccountsUri)
        .then()
            .statusCode(200)
            .contentType(ContentType.JSON)
            .body("$", empty<Any>())
    }

    private fun getAccountBalance(accountNumber: String): BigDecimal =
        accountRepository.getAccount(accountNumber).balance
}