## money-transfer-api

To run tests:

`./mvnw clean test`

To package application into one jar:

`./mvnw clean package`

After running the application will accept http requests on 7000 port.

Available API:

  - `GET /api/account/all` - returns all available accounts
  - `POST /api/account/transfer` - transfers specified amount from one account to another if it is possible 
